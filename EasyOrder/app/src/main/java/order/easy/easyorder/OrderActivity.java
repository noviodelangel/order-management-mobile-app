package order.easy.easyorder;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import order.easy.easyorder.helpers.Validator;
import order.easy.easyorder.model.Addresses;
import order.easy.easyorder.model.Order;
import order.easy.easyorder.model.OrderBaskets;
import order.easy.easyorder.model.Response;

public class OrderActivity extends BaseActivity {

    private String companyId;
    private OrderBaskets[] order;
    private Addresses address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_order);
        setHeading();

        companyId = intent.getStringExtra("companyId");

        Gson gson = new Gson();
        String orderJson = intent.getStringExtra("orderList");
        order = gson.fromJson(orderJson, OrderBaskets[].class);

        new AddressTask().execute();
    }

    public void order(View view) {

        EditText cityText = (EditText) findViewById(R.id.city_text);
        EditText streetText = (EditText) findViewById(R.id.street_text);
        EditText houseNumberText = (EditText) findViewById(R.id.house_number_text);
        EditText postalCodeText = (EditText) findViewById(R.id.postal_code_text);

        address = new Addresses();
        address.setCity(cityText.getText().toString());
        address.setStreet(streetText.getText().toString());
        address.setHouseNumber(houseNumberText.getText().toString());
        address.setPostalCode(postalCodeText.getText().toString());

        if (Validator.isAddressValid(address)) {
            Button button = (Button) findViewById(R.id.order_button);
            button.setClickable(false);

            new OrderTask().execute();
        }
        else {
            TextView responseText = (TextView) findViewById(R.id.response);
            responseText.setText("Podany adres jest błędny");
        }
    }

    private class AddressTask extends AsyncTask<Void, Void, Addresses> {

        @Override
        protected Addresses doInBackground(Void... params) {
            try {
                String url = "http://10.0.2.2:8080/api/customers/get-customer-address?id=" + customerId + "&session-number=" + sessionNumber;

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Addresses> responseEntity = restTemplate.getForEntity(url, Addresses.class);
                Addresses address = responseEntity.getBody();

                return address;
            } catch (Exception e) {
                Log.e("OrderActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Addresses address) {

            EditText cityText = (EditText) findViewById(R.id.city_text);
            EditText streetText = (EditText) findViewById(R.id.street_text);
            EditText houseNumberText = (EditText) findViewById(R.id.house_number_text);
            EditText postalCodeText = (EditText) findViewById(R.id.postal_code_text);

            cityText.setText(address.getCity());
            streetText.setText(address.getStreet());
            houseNumberText.setText(address.getHouseNumber());
            postalCodeText.setText(address.getPostalCode());
        }
    }

    private class OrderTask extends AsyncTask<Void, Void, Response> {

        @Override
        protected Response doInBackground(Void... params) {
            try {
                String url = "http://10.0.2.2:8080/api/orders/add-order?session-number=" + sessionNumber + "&customerid=" + customerId + "&companyid=" + companyId;
                RestTemplate restTemplate = new RestTemplate();

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                Order orderWithAddress = new Order();
                orderWithAddress.setOrderedItems(order);
                orderWithAddress.setAddress(address);

                Response response = restTemplate.postForObject(url, orderWithAddress, Response.class);

                return response;
            } catch (Exception e) {
                Log.e("OrderActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Response response) {
            TextView responseText = (TextView) findViewById(R.id.response);
            responseText.setText(response.getMessage());

            if (!response.getStatus()) {
                Button button = (Button) findViewById(R.id.order_button);
                button.setClickable(true);
            }
        }
    }
}
