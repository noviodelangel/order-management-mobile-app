package order.easy.easyorder.helpers;

import android.text.TextUtils;

import order.easy.easyorder.model.Addresses;
import order.easy.easyorder.model.Customers;

/**
 * Created by sosna on 25.11.2015.
 */
public class Validator {

    public static boolean isValidEmail(String target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isValidZipCode(String zipcode) {
        try {
            String[] splitted = zipcode.split("-");

            if (splitted.length == 2) {
                if (splitted[0].length() == 2 && splitted[1].length() == 3){
                    return true;
                }
            }
        }
        catch (Exception ex) {
            return false;
        }
        return false;
    }

    public static boolean isRegisterationFormValid(Customers customer, Addresses address) {
        if (customer == null || address == null)
            return false;

        if (isCustomerValid(customer) && isAddressValid(address)) {
            return true;
        }
        return false;
    }

    public static boolean isAddressValid(Addresses address) {
        if (!address.getCity().trim().isEmpty() &&
            !address.getStreet().trim().isEmpty() &&
            !address.getHouseNumber().trim().isEmpty()) {

            if (address.getPostalCode().trim().isEmpty() || isValidZipCode(address.getPostalCode().trim()))
                return true;
        }
        return false;
    }

    public static boolean isCustomerValid(Customers customer) {
        if (!customer.getName().trim().isEmpty() &&
                !customer.getSurname().trim().isEmpty() &&
                !customer.getTelephone().trim().isEmpty() &&
                isValidEmail(customer.getEmail()) &&
                !customer.getPassword().trim().isEmpty()) {

            return true;
        }
        return false;
    }
}
