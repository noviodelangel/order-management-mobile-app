package order.easy.easyorder;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import order.easy.easyorder.helpers.MenuAdapterItem;
import order.easy.easyorder.model.Menu;
import order.easy.easyorder.model.MenuDetails;
import order.easy.easyorder.model.OrderBaskets;

public class MenuListActivity extends BaseActivity {
    public String companyId;
    private List<Menu> menuList = new ArrayList<Menu>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu_list);
        setHeading();

        companyId = intent.getStringExtra("companyId");

        new MenuTask().execute();
    }

    public void order(View view) {

        OrderBaskets[] order = getOrderList();

        if (order.length == 0) {
            TextView responseText = (TextView) findViewById(R.id.response);
            responseText.setText("Wybierz dania");

            return;
        }

        Gson gson = new Gson();
        String orderJson = gson.toJson(order);

        Intent myIntent = new Intent(MenuListActivity.this, OrderActivity.class);
        myIntent.putExtra("sessionNumber", sessionNumber);
        myIntent.putExtra("companyId", companyId + "");
        myIntent.putExtra("customerId", customerId);
        myIntent.putExtra("orderList", orderJson);

        MenuListActivity.this.startActivity(myIntent);
    }

    private void fillListView(Menu[] menu) {
        ListView list = (ListView) findViewById(R.id.menu_list_view);

        for (Menu item : menu) {
            menuList.add(item);
        }
        MenuAdapterItem adapter = new MenuAdapterItem(this, R.layout.item_menu, menuList);

        list.setAdapter(adapter);
    }

    public OrderBaskets[] getOrderList(){
        //ListView list = (ListView) findViewById(R.id.menu_list_view);
        List<OrderBaskets> data = new ArrayList();
        ListView listView = (ListView) findViewById(R.id.menu_list_view);
        List<Menu> listData = ((MenuAdapterItem) listView.getAdapter()).getData();

        for (Menu item : listData) {
            for (MenuDetails detail : item.getDetails()) {
                OrderBaskets basket = new OrderBaskets();

                if (detail.getAmount() > 0) {
                    basket.setAmount(detail.getAmount());
                    basket.setDishId(detail.getDetail().getId());

                    data.add(basket);
                }
            }
        }

        return getFormattedOrder(data);
    }

    public OrderBaskets[] getFormattedOrder(List<OrderBaskets> data){
        OrderBaskets[] result = new OrderBaskets[data.size()];

        int counter = 0;
        for (OrderBaskets order : data){
            result[counter++] = order;
        }
        return result;
    }

    private class MenuTask extends AsyncTask<Void, Void, Menu[]> {

        @Override
        protected Menu[] doInBackground(Void... params) {
            try {
                String url = "http://10.0.2.2:8080/api/menu/get-menu?id=" + companyId  + "&session-number=" + sessionNumber  + "&customer-id=" + customerId;

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Menu[]> responseEntity = restTemplate.getForEntity(url, Menu[].class);
                Menu[] menu = responseEntity.getBody();

                return menu;
            } catch (Exception e) {
                Log.e("MenuListActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Menu[] menu) {
            fillListView(menu);
        }
    }
}
