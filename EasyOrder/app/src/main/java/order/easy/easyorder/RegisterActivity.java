package order.easy.easyorder;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import order.easy.easyorder.helpers.CustomerHelper;
import order.easy.easyorder.helpers.Validator;
import order.easy.easyorder.model.Addresses;
import order.easy.easyorder.model.CustomerWithAddress;
import order.easy.easyorder.model.Customers;
import order.easy.easyorder.model.Response;

public class RegisterActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Customers customer;
    private Addresses address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
    }

    public void register(View view) {

        EditText emailText = (EditText) findViewById(R.id.email_text);
        EditText passwordText = (EditText) findViewById(R.id.password_text);
        EditText nameText = (EditText) findViewById(R.id.name_text);
        EditText surnameText = (EditText) findViewById(R.id.surname_text);
        EditText telephoneText = (EditText) findViewById(R.id.telephone_text);

        EditText cityText = (EditText) findViewById(R.id.city_text);
        EditText streetText = (EditText) findViewById(R.id.street_text);
        EditText houseNumberText = (EditText) findViewById(R.id.house_number_text);
        EditText postalCodeText = (EditText) findViewById(R.id.postal_code_text);

        customer = new Customers();
        address = new Addresses();

        customer.setEmail(emailText.getText().toString());
        customer.setPassword(CustomerHelper.hash(passwordText.getText().toString()));
        customer.setName(nameText.getText().toString());
        customer.setSurname(surnameText.getText().toString());
        customer.setTelephone(telephoneText.getText().toString());

        address.setCity(cityText.getText().toString());
        address.setStreet(streetText.getText().toString());
        address.setHouseNumber(houseNumberText.getText().toString());
        address.setPostalCode(postalCodeText.getText().toString());

        if (Validator.isRegisterationFormValid(customer, address)) {
            new RegisterTask().execute();
        }
        else {
            TextView responseText = (TextView) findViewById(R.id.response);
            responseText.setText("Dane są nieprawidłowe");
        }
    }

    private class RegisterTask extends AsyncTask<Void, Void, Response> {

        @Override
        protected Response doInBackground(Void... params) {
            try {
                String url = "http://10.0.2.2:8080/api/customers/add-customer";
                RestTemplate restTemplate = new RestTemplate();

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                CustomerWithAddress customerWithAddress = new CustomerWithAddress();
                customerWithAddress.setCustomer(customer);
                customerWithAddress.setAddress(address);

                Response response = restTemplate.postForObject(url, customerWithAddress, Response.class);

                return response;
            } catch (Exception e) {
                Log.e("RegisterActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Response response) {
            if (response.getStatus()) {
                Intent myIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                myIntent.putExtra("response", response.getMessage());

                RegisterActivity.this.startActivity(myIntent);
            }
            else {
                TextView responseText = (TextView) findViewById(R.id.response);
                responseText.setText(response.getMessage());
            }
        }
    }
}
