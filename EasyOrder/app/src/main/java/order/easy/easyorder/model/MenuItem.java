package order.easy.easyorder.model;

import java.util.List;

/**
 * Created by sosna on 24.10.2015.
 */

public class MenuItem {

    private int _id;
    private String _name;
    private List<DishDetails> _details;

    public int getId() {
        return _id;
    }

    public String getName() {
        return _name;
    }

    public List<DishDetails> getDetails() {
        return _details;
    }

    public MenuItem(int itemId, String itemName, List<DishDetails> itemDetails) {
        _id = itemId;
        _name = itemName;
        _details = itemDetails;
    }
}