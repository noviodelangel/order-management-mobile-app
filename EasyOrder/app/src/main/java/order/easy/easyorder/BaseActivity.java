package order.easy.easyorder;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import order.easy.easyorder.helpers.OrderHelper;

/**
 * Created by sosna on 20.11.2015.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Toolbar toolbar;
    protected String sessionNumber;
    protected String customerId;
    protected Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        intent = getIntent();
        sessionNumber = intent.getStringExtra("sessionNumber");
        customerId = intent.getStringExtra("customerId");
    }

    @Override
    protected void onStart() {
        super.onStart();
        
        new StatusTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_order_status) {
            Intent myIntent = new Intent(this, OrderStatusActivity.class);
            myIntent.putExtra("sessionNumber", sessionNumber);
            myIntent.putExtra("customerId", customerId);
            this.startActivity(myIntent);
        }
        if (id == R.id.menu_account_edit) {
            Intent myIntent = new Intent(this, AccountEditActivity.class);
            myIntent.putExtra("sessionNumber", sessionNumber);
            myIntent.putExtra("customerId", customerId);
            this.startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    protected void setHeading() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
    }

    private class StatusTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {

            try {
                String url = "http://10.0.2.2:8080/api/orders/get-not-shown-order-status?customer-id=" + customerId + "&session-number=" + sessionNumber;

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Integer> responseEntity = restTemplate.getForEntity(url, Integer.class);
                Integer status = responseEntity.getBody();

                return status;
            } catch (Exception e) {
                Log.e("BaseActivity", e.getMessage(), e);
            }

            return -1;
        }

        @Override
        protected void onPostExecute(Integer status) {

            if (status >= 0) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(BaseActivity.this);
                dlgAlert.setMessage(OrderHelper.getStatusMessage(status));
                dlgAlert.setTitle("Zamówienie");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
            }
        }
    }
}
