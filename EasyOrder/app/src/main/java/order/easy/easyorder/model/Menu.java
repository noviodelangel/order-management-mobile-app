package order.easy.easyorder.model;

import java.util.List;

/**
 * Created by sosna on 12.10.2015.
 */
public class Menu {
    private Dishes dish;
    private List<MenuDetails> details;

    public List<MenuDetails> getDetails() {
        return details;
    }

    public void setDetails(List<MenuDetails> details) {
        this.details = details;
    }

    public Dishes getDish() {
        return dish;
    }
}
