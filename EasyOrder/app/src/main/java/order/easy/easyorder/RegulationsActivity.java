package order.easy.easyorder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

public class RegulationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regulations);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        TextView regulationsText = (TextView) findViewById(R.id.regulations);
        regulationsText.setMovementMethod(new ScrollingMovementMethod());
    }

    public void register(View view) {
        Intent intent = new Intent(RegulationsActivity.this, RegisterActivity.class);
        RegulationsActivity.this.startActivity(intent);
    }
}
