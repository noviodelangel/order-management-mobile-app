package order.easy.easyorder.helpers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import order.easy.easyorder.R;
import order.easy.easyorder.model.Menu;
import order.easy.easyorder.model.MenuDetails;

public class MenuAdapterItem extends ArrayAdapter<Menu> {
    Context mContext;
    int layoutResourceId;
    public List<Menu> data = null;
    private List<ListView> details;

    public List<Menu> getData() {
        return data;
    }

    public MenuAdapterItem(Context mContext, int layoutResourceId, List<Menu> data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (details == null) {
            details = new ArrayList<ListView>();

            for (Menu item : data) {
                details.add(null);
            }
        }
        else {
            for (ListView detailsList : details) {

                if (detailsList == null) continue;

                List<MenuDetails> detail = ((MenuDetailsAdapterItem)detailsList.getAdapter()).getData();

                int i = 0;
                int dishId = detail.get(0).getDetail().getDishId();
                for (Menu item : data) {
                    if (item.getDish().getId() == dishId) {
                        data.get(i).setDetails(detail);
                        break;
                    }
                    i++;
                }
            }
        }

        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
        Menu menuItem = data.get(position);

        TextView textViewItem = (TextView) convertView.findViewById(R.id.list_view_item);
        textViewItem.setText(menuItem.getDish().getName());
        textViewItem.setTag(menuItem.getDish().getId() + "");

        TextView description = (TextView) convertView.findViewById(R.id.list_view_description);
        description.setText(menuItem.getDish().getDescription());

        ListView list = (ListView) convertView.findViewById(R.id.menu_details_list_view);

        ViewGroup.LayoutParams layoutParams = list.getLayoutParams();
        layoutParams.height = 152 * menuItem.getDetails().size();
        list.setLayoutParams(layoutParams);

        MenuDetailsAdapterItem adapter = new MenuDetailsAdapterItem(mContext, R.layout.item_menu_details, menuItem.getDetails());

        list.setAdapter(adapter);
        details.set(position, list);

        return convertView;
    }
}
