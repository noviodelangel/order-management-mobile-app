package order.easy.easyorder.model;

public class Customers {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String telephone;
    private String sessionNumber;
    private int addressId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getSessionNumber() {
        return sessionNumber;
    }

    public void setSessionNumber(String sessionNumber) {
        this.sessionNumber = sessionNumber;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customers customers = (Customers) o;

        if (id != customers.id) return false;
        if (addressId != customers.addressId) return false;
        if (name != null ? !name.equals(customers.name) : customers.name != null) return false;
        if (surname != null ? !surname.equals(customers.surname) : customers.surname != null) return false;
        if (email != null ? !email.equals(customers.email) : customers.email != null) return false;
        if (password != null ? !password.equals(customers.password) : customers.password != null) return false;
        if (telephone != null ? !telephone.equals(customers.telephone) : customers.telephone != null) return false;
        if (sessionNumber != null ? !sessionNumber.equals(customers.sessionNumber) : customers.sessionNumber != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (sessionNumber != null ? sessionNumber.hashCode() : 0);
        result = 31 * result + addressId;
        return result;
    }
}
