package order.easy.easyorder;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import order.easy.easyorder.helpers.OrderStatusAdapterItem;
import order.easy.easyorder.model.OrderStatus;

public class OrderStatusActivity extends BaseActivity {

    private List<OrderStatus> orderStatusList = new ArrayList<OrderStatus>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_order_status);
        setHeading();

        new OrderStatusListTask().execute();
    }

    private class OrderStatusListTask extends AsyncTask<Void, Void, OrderStatus[]> {

        @Override
        protected OrderStatus[] doInBackground(Void... params) {
            try {
                String url = "http://10.0.2.2:8080/api/orders/get-customer-order-statuses?customerId=" + customerId + "&session-number=" + sessionNumber;

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<OrderStatus[]> responseEntity = restTemplate.getForEntity(url, OrderStatus[].class);
                OrderStatus[] orderStatuses = responseEntity.getBody();

                return orderStatuses;
            } catch (Exception e) {
                Log.e("OrderStatusActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(OrderStatus[] orderStatus) {
            fillListView(orderStatus);
        }
    }

    private void fillListView(OrderStatus[] orderStatus){

        for (OrderStatus order : orderStatus) {
            orderStatusList.add(order);
        }

        ListView list = (ListView) findViewById(R.id.order_status_list);

        OrderStatusAdapterItem adapter = new OrderStatusAdapterItem(this, R.layout.item_order_status, orderStatusList);
        list.setAdapter(adapter);
    }
}
