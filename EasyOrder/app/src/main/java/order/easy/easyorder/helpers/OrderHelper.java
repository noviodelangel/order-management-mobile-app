package order.easy.easyorder.helpers;

/**
 * Created by sosna on 26.11.2015.
 */
public class OrderHelper {

    public static String getStatus(int id) {

        switch(id){
            case 0:
                return "Zamówienie złożone";
            case 1:
                return "Zaakceptowane";
            case 2:
                return "Przygotowywane";
            case 3:
                return "W trakcie dostawy";
            case 4:
                return "Dostarczone";
        }
        return "";
    }

    public static String getStatusMessage(int id) {

        switch(id){
            case 0:
                return "Twoje zamówienie zostało złożone";
            case 1:
                return "Twoje zamówienie zostało zaakceptowane";
            case 2:
                return "Twoje zamówienie jest przygotowywane";
            case 3:
                return "Twoje zamówienie jest w trakcie dostawy";
            case 4:
                return "Twoje zamówienie zostało dostarczone";
        }
        return "";
    }
}
