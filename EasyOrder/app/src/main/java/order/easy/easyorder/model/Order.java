package order.easy.easyorder.model;

/**
 * Created by sosna on 18.11.2015.
 */
public class Order {
    private OrderBaskets[] orderedItems;
    private Addresses address;

    public Addresses getAddress() {
        return address;
    }

    public void setAddress(Addresses address) {
        this.address = address;
    }

    public OrderBaskets[] getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(OrderBaskets[] orderedItems) {
        this.orderedItems = orderedItems;
    }
}
