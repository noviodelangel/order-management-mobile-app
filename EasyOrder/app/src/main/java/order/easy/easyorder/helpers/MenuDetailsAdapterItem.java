package order.easy.easyorder.helpers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import order.easy.easyorder.R;
import order.easy.easyorder.model.MenuDetails;

public class MenuDetailsAdapterItem extends ArrayAdapter<MenuDetails> {

    Context mContext;
    int layoutResourceId;
    List<MenuDetails> data = null;

    public List<MenuDetails> getData() {
        return data;
    }

    public MenuDetailsAdapterItem(Context mContext, int layoutResourceId, List<MenuDetails> data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
        MenuDetails details = data.get(position);

        TextView textViewName = (TextView) convertView.findViewById(R.id.menu_details_name);
        textViewName.setText(details.getDetail().getSize());
        textViewName.setTag(details.getDetail().getId() + "");

        TextView textViewPrice = (TextView) convertView.findViewById(R.id.menu_details_price);
        textViewPrice.setText(details.getDetail().getPrice() + "zł");

        TextView priceTextView = ((TextView) convertView.findViewById(R.id.menu_details_price));
        TextView amountTextView = ((TextView) convertView.findViewById(R.id.menu_details_amount));
        amountTextView.setTag(details.getDetail().getId() + "");
        amountTextView.setText(details.getAmount() + "");

        Button buttonPlus = (Button) convertView.findViewById(R.id.button_plus);
        buttonPlus.setOnClickListener(new ButtonPlusOnClick(amountTextView, priceTextView, textViewName, position, mContext));

        Button buttonMinus = (Button) convertView.findViewById(R.id.button_minus);
        buttonMinus.setOnClickListener(new ButtonMinusOnClick(amountTextView, priceTextView, textViewName, position, mContext));

        return convertView;
    }

    public class ButtonPlusOnClick implements AdapterView.OnClickListener {
        private TextView _amountTextView;
        private TextView _priceTextView;
        private Context _context;
        private int _id;
        private int _position;

        public ButtonPlusOnClick(TextView amount, TextView price, TextView name, int position, Context context){
            _amountTextView = amount;
            _priceTextView = price;
            _context = context;
            _id = Integer.parseInt(name.getTag().toString());
            _position = position;
        }

        @Override
        public void onClick(View view) {
            int amount = Integer.parseInt(_amountTextView.getText().toString()) + 1;
            double price =  Double.parseDouble(_priceTextView.getText().toString().replace("zł", ""));
            _amountTextView.setText(amount + "");

            TextView costTextView = (TextView)((Activity)_context).getWindow().getDecorView().findViewById(R.id.order_total_cost);

            double totalCost = Double.parseDouble(costTextView.getTag().toString());
            totalCost += price;

            costTextView.setTag(totalCost + "");
            costTextView.setText("Suma: " + totalCost + "zł");

            for (MenuDetails detail : data) {
                if (detail.getDetail().getId() == _id) {
                    data.get(_position).setAmount(amount);
                }
            }
        }
    }

    public class ButtonMinusOnClick implements AdapterView.OnClickListener {
        private TextView _amountTextView;
        private TextView _priceTextView;
        private Context _context;
        private int _id;
        private int _position;

        public ButtonMinusOnClick(TextView amount, TextView price, TextView name, int position, Context context) {
            _amountTextView = amount;
            _priceTextView = price;
            _context = context;
            _id = Integer.parseInt(name.getTag().toString());
            _position = position;
        }

        @Override
        public void onClick(View view) {
            int amount = Integer.parseInt(_amountTextView.getText().toString()) - 1;
            if (amount >= 0) {
                double price =  Double.parseDouble(_priceTextView.getText().toString().replace("zł", ""));
                _amountTextView.setText(amount + "");

                TextView costTextView = (TextView)((Activity)_context).getWindow().getDecorView().findViewById(R.id.order_total_cost);

                double totalCost = Double.parseDouble(costTextView.getTag().toString());
                totalCost -= price;

                costTextView.setTag(totalCost + "");
                costTextView.setText("Suma: " + totalCost + "zł");

                for (MenuDetails detail : data) {
                    if (detail.getDetail().getId() == _id) {
                        data.get(_position).setAmount(amount);
                    }
                }
            }
        }
    }
}
