package order.easy.easyorder.helpers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import order.easy.easyorder.R;
import order.easy.easyorder.model.OrderStatus;

public class OrderStatusAdapterItem extends ArrayAdapter<OrderStatus> {
    Context mContext;
    int layoutResourceId;
    List<OrderStatus> data = null;

    public OrderStatusAdapterItem(Context mContext, int layoutResourceId, List<OrderStatus> data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
        OrderStatus ordersStatus = data.get(position);

        TextView statusText = (TextView) convertView.findViewById(R.id.status);
        TextView detailsText = (TextView) convertView.findViewById(R.id.details);
        TextView dateText = (TextView) convertView.findViewById(R.id.date);
        TextView companyText = (TextView) convertView.findViewById(R.id.company);
        TextView paymentText = (TextView) convertView.findViewById(R.id.payment);

        statusText.setText(OrderHelper.getStatus(ordersStatus.getStatus()));

        detailsText.setText(ordersStatus.getDetails());

        if (ordersStatus.getDeliveryDate() != null) {
            try {
                String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(ordersStatus.getDeliveryDate());
                dateText.setText("Termin dostawy: " + formattedDate);
            }
            catch (Exception ex)
            {
                dateText.setText("Termin dostawy: brak");
            }
        }
        else {
            dateText.setText("Termin dostawy: brak");
        }

        companyText.setText(ordersStatus.getCompanyName());
        paymentText.setText("Kwota: " + ordersStatus.getPayment() + "zł");

        return convertView;
    }
}
