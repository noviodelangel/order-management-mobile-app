package order.easy.easyorder.model;

/**
 * Created by sosna on 24.10.2015.
 */

public class RestaurantItem {

    public int itemId;
    public String itemName;

    public RestaurantItem(int itemId, String itemName) {
        this.itemId = itemId;
        this.itemName = itemName;
    }
}