package order.easy.easyorder.model;

/**
 * Created by sosna on 01.12.2015.
 */
public class MenuDetails {

    private DishDetails detail;
    private int amount;

    public DishDetails getDetail() {
        return detail;
    }

    public void setDetail(DishDetails detail) {
        this.detail = detail;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
