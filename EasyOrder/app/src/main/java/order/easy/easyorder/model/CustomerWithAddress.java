package order.easy.easyorder.model;

/**
 * Created by sosna on 21.11.2015.
 */
public class CustomerWithAddress {

    private Customers customer;
    private Addresses address;

    public Customers getCustomer() {
        return customer;
    }

    public void setCustomer(Customers customer) {
        this.customer = customer;
    }

    public Addresses getAddress() {
        return address;
    }

    public void setAddress(Addresses address) {
        this.address = address;
    }
}
