package order.easy.easyorder;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import order.easy.easyorder.helpers.RestaurantsAdapterItem;
import order.easy.easyorder.model.Companies;
import order.easy.easyorder.model.RestaurantItem;

public class RestaurantsListActivity extends BaseActivity {

    private List<RestaurantItem> restaurantList = new ArrayList<RestaurantItem>();
    private Companies[] companiesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_restaurants_list);
        setHeading();

        new RestaurantsListTask().execute();
    }

    private class RestaurantsListTask extends AsyncTask<Void, Void, Companies[]> {

        @Override
        protected Companies[] doInBackground(Void... params) {
            try {
                String url = "http://10.0.2.2:8080/api/restaurants/get-all-restaurants?session-number=" + sessionNumber  + "&customer-id=" + customerId;

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Companies[]> responseEntity = restTemplate.getForEntity(url, Companies[].class);
                Companies[] companies = responseEntity.getBody();

                return companies;
            } catch (Exception e) {
                Log.e("RestaurantsListActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Companies[] companies) {
            fillListView(companies);
        }
    }

    private void fillListView(Companies[] companies){
        companiesList = companies;
        ListView list = (ListView) findViewById(R.id.restaurants_list);

        for (Companies company : companies){
            if (company.getStatus() == true) {
                restaurantList.add(new RestaurantItem(company.getId(), company.getName()));
            }
        }
        RestaurantsAdapterItem adapter = new RestaurantsAdapterItem(this, R.layout.item_restaurant, restaurantList);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new OnItemClickListenerListViewItem());
    }

    public class OnItemClickListenerListViewItem implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            TextView textViewItem = ((TextView) view.findViewById(R.id.list_view_item));

            String listItemId = textViewItem.getTag().toString();

            int companyId = companiesList[Integer.parseInt(listItemId) - 1].getId();

            Intent myIntent = new Intent(RestaurantsListActivity.this, MenuListActivity.class);
            myIntent.putExtra("sessionNumber", sessionNumber);
            myIntent.putExtra("companyId", companyId + "");
            myIntent.putExtra("customerId", customerId);
            RestaurantsListActivity.this.startActivity(myIntent);
        }
    }
}