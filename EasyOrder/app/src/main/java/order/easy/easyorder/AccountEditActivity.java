package order.easy.easyorder;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import order.easy.easyorder.helpers.CustomerHelper;
import order.easy.easyorder.helpers.Validator;
import order.easy.easyorder.model.Addresses;
import order.easy.easyorder.model.CustomerWithAddress;
import order.easy.easyorder.model.Customers;
import order.easy.easyorder.model.Response;

public class AccountEditActivity extends BaseActivity {

    private Customers customer;
    private Addresses address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_account_edit);
        setHeading();

        new CustomerDataTask().execute();
    }

    public void accountEdit(View view) {

        EditText emailText = (EditText) findViewById(R.id.email_text);
        EditText passwordText = (EditText) findViewById(R.id.password_text);
        EditText nameText = (EditText) findViewById(R.id.name_text);
        EditText surnameText = (EditText) findViewById(R.id.surname_text);
        EditText telephoneText = (EditText) findViewById(R.id.telephone_text);

        EditText cityText = (EditText) findViewById(R.id.city_text);
        EditText streetText = (EditText) findViewById(R.id.street_text);
        EditText houseNumberText = (EditText) findViewById(R.id.house_number_text);
        EditText postalCodeText = (EditText) findViewById(R.id.postal_code_text);

        customer.setEmail(emailText.getText().toString());
        customer.setPassword(CustomerHelper.hash(passwordText.getText().toString()));
        customer.setName(nameText.getText().toString());
        customer.setSurname(surnameText.getText().toString());
        customer.setTelephone(telephoneText.getText().toString());

        address.setCity(cityText.getText().toString());
        address.setStreet(streetText.getText().toString());
        address.setHouseNumber(houseNumberText.getText().toString());
        address.setPostalCode(postalCodeText.getText().toString());

        if (Validator.isRegisterationFormValid(customer, address)) {
            new AccountEditTask().execute();
        }
        else {
            TextView responseText = (TextView) findViewById(R.id.response);
            responseText.setText("Dane są nieprawidłowe");
        }
    }

    private class CustomerDataTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                String customerUrl = "http://10.0.2.2:8080/api/customers/get-customer?id=" + customerId + "&session-number=" + sessionNumber;
                String addressUrl = "http://10.0.2.2:8080/api/customers/get-customer-address?id=" + customerId + "&session-number=" + sessionNumber;

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Customers> responseCustomerEntity = restTemplate.getForEntity(customerUrl, Customers.class);
                customer = responseCustomerEntity.getBody();

                ResponseEntity<Addresses> responseAddressEntity = restTemplate.getForEntity(addressUrl, Addresses.class);
                address = responseAddressEntity.getBody();

                return true;
            } catch (Exception e) {
                Log.e("AccountEditActivity", e.getMessage(), e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean status) {

            EditText emailText = (EditText) findViewById(R.id.email_text);
            EditText nameText = (EditText) findViewById(R.id.name_text);
            EditText surnameText = (EditText) findViewById(R.id.surname_text);
            EditText telephoneText = (EditText) findViewById(R.id.telephone_text);

            EditText cityText = (EditText) findViewById(R.id.city_text);
            EditText streetText = (EditText) findViewById(R.id.street_text);
            EditText houseNumberText = (EditText) findViewById(R.id.house_number_text);
            EditText postalCodeText = (EditText) findViewById(R.id.postal_code_text);

            emailText.setText(customer.getEmail());
            nameText.setText(customer.getName());
            surnameText.setText(customer.getSurname());
            telephoneText.setText(customer.getTelephone());

            cityText.setText(address.getCity());
            streetText.setText(address.getStreet());
            houseNumberText.setText(address.getHouseNumber());
            postalCodeText.setText(address.getPostalCode());
        }
    }

    private class AccountEditTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            try {
                String url = "http://10.0.2.2:8080/api/customers/update-customer-with-address?session-number=" + sessionNumber + "&customer-id=" + customerId;
                RestTemplate restTemplate = new RestTemplate();

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                CustomerWithAddress customerWithAddress = new CustomerWithAddress();
                customerWithAddress.setCustomer(customer);
                customerWithAddress.setAddress(address);

                Response response = restTemplate.postForObject(url, customerWithAddress, Response.class);

                return response.getMessage();
            } catch (Exception e) {
                Log.e("AccountEditActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            TextView responseText = (TextView) findViewById(R.id.response);
            responseText.setText(response);
        }
    }
}
