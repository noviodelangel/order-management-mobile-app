package order.easy.easyorder;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import order.easy.easyorder.helpers.CustomerHelper;
import order.easy.easyorder.model.Customers;

public class LoginActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_main);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String response = intent.getStringExtra("response");

        if (response != null) {
            TextView responseText = (TextView) findViewById(R.id.response);
            responseText.setText(response);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void login(View view) {

        EditText emailText = (EditText) findViewById(R.id.emailTextBox);
        EditText passText = (EditText) findViewById(R.id.passTextBox);
        String email = emailText.getText().toString();
        String pass = passText.getText().toString();

        LoginTask loginTask = new LoginTask();
        loginTask.email = email;
        loginTask.pass = pass;

        loginTask.execute();
    }

    public void register(View view) {
        Intent intent = new Intent(LoginActivity.this, RegulationsActivity.class);
        LoginActivity.this.startActivity(intent);
    }

    private class LoginTask extends AsyncTask<Void, Void, Customers> {
        public String email;
        public String pass;

        @Override
        protected Customers doInBackground(Void... params) {
            try {
                pass = CustomerHelper.hash(pass);
                String url = "http://10.0.2.2:8080/api/customers/login?email=" + email + "&password=" + pass;

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                Customers customer = restTemplate.getForObject(url, Customers.class);
                return customer;
            } catch (Exception e) {
                Log.e("LoginActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Customers customer) {

            if (!customer.getSessionNumber().equals("0")) {
                Intent myIntent = new Intent(LoginActivity.this, RestaurantsListActivity.class);
                myIntent.putExtra("sessionNumber", customer.getSessionNumber());
                myIntent.putExtra("customerId", customer.getId() + "");
                LoginActivity.this.startActivity(myIntent);
            }
            else {
                TextView responseText = (TextView) findViewById(R.id.response);
                responseText.setText("Podane dane są nieprawidłowe");
            }
        }
    }
}
