package order.easy.easyorder.helpers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import order.easy.easyorder.R;
import order.easy.easyorder.model.RestaurantItem;

public class RestaurantsAdapterItem extends ArrayAdapter<RestaurantItem> {
    Context mContext;
    int layoutResourceId;
    List<RestaurantItem> data = null;

    public RestaurantsAdapterItem(Context mContext, int layoutResourceId, List<RestaurantItem> data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
        RestaurantItem RestaurantItem = data.get(position);

        TextView textViewItem = (TextView) convertView.findViewById(R.id.list_view_item);
        textViewItem.setText(RestaurantItem.itemName);
        textViewItem.setTag(RestaurantItem.itemId + "");

        return convertView;
    }
}
